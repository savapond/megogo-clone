const API_KEY = 'b60b2035854ab1de963b375722698262';

const requests = {
  fetchAllMovies: `/discover/tv?api_key=${API_KEY}&with_networks`,
  fetchTrending: `/trending/all/week?api_key=${API_KEY}&language=en-US`,
  fetchNetflixOriginals: `/discover/tv?api_key=${API_KEY}&with_networks=213`,
  fetchTopRated: `/movie/top_rated?api_key=${API_KEY}&language=en-US`,
  fetchActionMovies: `/discover/movie?api_key=${API_KEY}&with_genres=28`,
  fetchComedyMovies: `/discover/movie?api_key=${API_KEY}&with_genres=35`,
  fetchHorrorMovies: `/discover/movie?api_key=${API_KEY}&with_genres=27`,
  fetchRomanceMovies: `/discover/movie?api_key=${API_KEY}&with_genres=10749`,
  fetchDocumentaries: `/discover/movie?api_key=${API_KEY}&with_genres=99`,
};

export {requests};
// https://api.themoviedb.org/3/discover/tv?api_key=b60b2035854ab1de963b375722698262&with_networks=213

const BASE_URL = 'https://api.themoviedb.org/3';

class MoviesServices {
  static async fetchAllMovies() {
    return fetch(`${BASE_URL}${requests.fetchAllMovies}`)
        .then((data)=>data.json());
  }
  static async fetchTrending() {
    return fetch(`${BASE_URL}${requests.fetchTrending}`)
        .then((data)=>data.json());
  }

  static fetchNetflixOriginals() {
    return fetch(`${BASE_URL}${requests.fetchNetflixOriginals}`)
        .then((data) => data.json());
  }
  static fetchTopRated() {
    return fetch(`${BASE_URL}${requests.fetchTopRated}`)
        .then((data) => data.json());
  }
  static fetchActionMovies() {
    return fetch(`${BASE_URL}${requests.fetchActionMovies}`)
        .then((data) => data.json());
  }
  static fetchRomanceMovies() {
    return fetch(`${BASE_URL}${requests.fetchRomanceMovies}`)
        .then((data) => data.json());
  }
  static fetchHorrorMovies() {
    return fetch(`${BASE_URL}${requests.fetchHorrorMovies}`)
        .then((data) => data.json());
  }
  static fetchDocumentaries() {
    return fetch(`${BASE_URL}${requests.fetchDocumentaries}`)
        .then((data) => data.json());
  }
  static fetchComedyMovies() {
    return fetch(`${BASE_URL}${requests.fetchComedyMovies}`)
        .then((data) => data.json());
  }
}
export default MoviesServices;
