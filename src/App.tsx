import React from 'react';
import './App.scss';
import {Route, Routes} from 'react-router-dom';
import HomePage from './pages/HomePages';
import SinglePageMovie from './pages/SinglePageMovie';

function App() {
  return <div className="App">
    <Routes>
      <Route path="/" element={<HomePage/>}/>
      <Route path="/movie/:id" element={<SinglePageMovie />} />
    </Routes>

  </div>;
}

export default App;
