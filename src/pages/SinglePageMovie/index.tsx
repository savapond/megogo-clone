import React, {useEffect, useState} from 'react';
// import Banner from './Banner';
import {useParams} from 'react-router-dom';
import MoviesServices from '../../services/APIServices';
import {Movie} from '../../models/movie.model';
import Header from '../../common/components/header';

const SinglePageMovie: React.FC = () => {
  const {id} = useParams();
  const [movie, setMovie] = useState<Movie | null>( null);
  const baseUrlImg = 'https://image.tmdb.org/t/p/original';
  useEffect( () => {
    MoviesServices.fetchAllMovies()
        .then((movie:any)=>setMovie( movie.results.find((item:any) => item.id == id)));
  }, [setMovie]);
  console.log('singl', movie);
  return (
    <>
      <Header/>
      <h2>{ movie?.title || movie?.name || movie?.original_title}</h2>
      <img src={`${baseUrlImg}/${movie?.backdrop_path}`} alt=""/>
      <div>
        {movie?.overview} <br/>
     original language: {movie?.original_language} <br/>
        rate: {movie?.vote_average}
      </div>

    </>
  );
};
export default SinglePageMovie;
