import React, {useEffect, useState} from 'react';
import MoviesServices from '../../services/APIServices';
import {Navigation, Pagination, Autoplay} from 'swiper';
import {Swiper, SwiperSlide} from 'swiper/react';
import 'swiper/scss';
import 'swiper/scss/navigation';
import 'swiper/scss/pagination';
import './style.scss';
import {Link} from 'react-router-dom';


const HomeSlider: React.FC = () => {
  const [movies, setMovies] = useState([]);
  const baseUrlImg = 'https://image.tmdb.org/t/p/original';
  useEffect(() => {
    MoviesServices.fetchTrending()
        .then((movie:any) => setMovies(movie.results));
  }, [setMovies]);
  return (
    <div className='slider__main'>
      <Swiper
        modules={[Navigation, Pagination, Autoplay]}
        navigation
        pagination={{clickable: true}}
        autoplay={{
          delay: 2000,
        }}
      >
        {movies.map((movie: any, index: number) => (
          <SwiperSlide key={index} style={{
            backgroundImage: `url(${baseUrlImg}/${movie?.backdrop_path})`,
            backgroundSize: 'cover',
            backgroundPosition: '50% 50%',
          }}>
            <div className="slide__content">
              <Link to={`/movie/${movie.id}`} key={movie.id}>
                <h1 className="slide__title">{movie?.name || movie?.title || movie?.original_title}</h1>
              </Link>

              <div className="slide__desc">
                {movie.overview}
              </div>
            </div>
          </SwiperSlide>

        ))}
      </Swiper>
    </div>
  );
};

export default HomeSlider;
